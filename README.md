# Graph Theory Project

Quentin ROSTALSKI / Matthias URANGA
<br><br>

## Index
***

- [Graph Theory Project](#graph-theory-project)
  - [Index](#index)
  - [Subject](#subject)
  - [Objective](#objective)
  - [Specifications](#specifications)
  - [Our choice](#our-choice)
  - [Work organisation](#work-organisation)
  - [User Manual](#user-manual)
- [Stable Marriage Algorithm](#stable-marriage-algorithm)
  - [Files](#files)
    - [Python files](#python-files)
      - [Create json file](#create-json-file)
      - [Load json file](#load-json-file)
      - [stableMarriageAlgo file](#stablemarriagealgo-file)
    - [Example JSON files](#example-json-files)
      - [Students JSON](#students-json)
      - [Schools JSON](#schools-json)
      - [Result of stable mariage JSON](#result-of-stable-mariage-json)


## Subject
***
Some students want to choose schools for next year. Each student ranked the schools and each school ranked each student. The goal is to find a combination that would be as stable as possible according to each student's preferences.
<br><br>

## Objective
***

The objective of the project is to realize a stable marriage algorithm between students and schools in the language of our choice. 
<br><br>

## Specifications
***

- Students and schools and their choices must be specified in files.
- Students or Schools can serenade
- There is a defined number of place in every school
- For a stable mariage, we need the same numbers of students than the total numbers of place in schools
<br><br>

## Our choice
***

We choose to developp the algorithm in **Python** which is very useful for ease of programming as well as ease of understanding, the source file and the result of the algorithm will be in **json** because it's a widely used format nowadays and it's very easy to understand and couple it with Python. The project is divided in 4 parts :
- A programm to create the **json** file randomly.
- A programm to read the **json** file.
- A programm used for the user menu.
- A programm to apply the algorithm. The algorithm is also divided in multiples parts :
  - A function *makeMatches*, which allows you to assign the student to the first school on your preferred school list by removing that school from your list after assigning the student.
  - A function *checkMatches*, which makes it possible to see for each school whether the number of students applying per round is greater than their capacity. If this is the case then the school will sort the students according to its preference list and will eject the excess students.
  - A function *resultToJson*, which transform the result into a JSON file.
<br><br>

## Work organisation
***
We decided to work with GitLab in order to be able to work in parallel and to be able to control and version our files.
<br><br>

## User Manual
***
To start the program you have to execute the **main.py** file. Once launched you will come across the following window :

<pre>
<b>#############################
# Stable Marriage Algorithm #
#############################
What do you want to do ?
(St)udents serenading
(Sc)hool serenading
Choice ((St) or (Sc)) :</b> 
</pre>

From here you can choose who is going to serenade (St)udents or (Sc)hools.



Finally, you'll find the result in the file **result.json**.
<br><br>

## Files

### Python files

#### Create json file
***

```python
import random
import json

def init_Students_Schools():
    # Asking the user how many students he wants
    nbStudents = int(input("Number of Students : "))

    # Asking the user how many schools he wants
    nbSchools = int(input("Number of Schools : "))
    return [nbStudents,nbSchools]


def createSchools(list_init):
    # Init the random students list
    random_student_list = []
    for i in range(list_init[0]):
        random_student_list.append(i)
    
    # Init the random schools list
    random_school_list = []
    for i in range(list_init[1]):
        random_school_list.append(i)

    # Init Students and their choices
    students_dict = {}
    for i in range(list_init[0]):
        choice = []
        random.shuffle(random_school_list)
        for j in range(len(random_school_list)):
            choice.append("school" + str(random_school_list[j]))
        students_dict["stud" + str(i)] = choice
    
    # Init Schools and their choices
    schools_dict = {}
    for i in range(list_init[1]):
        choice = []
        random.shuffle(random_student_list)
        for j in range(len(random_student_list)):
            choice.append("stud" + str(random_student_list[j]))
        schools_dict["school" + str(i)] = choice

    # Write students and them choices in json file
    with open("student.json","w") as file:
        file.write(json.dumps(students_dict, indent=4))

    # Write schools and them choices in json file
    with open("school.json","w") as file:
        file.write(json.dumps(schools_dict, indent=4))
```
<br>

#### Load json file
***

```python
import json

def load():
    # Open the school json file and transform json type into dict type
    with open("school.json") as file:
        school_dict = json.loads(file.read())

    # Open the student json file and transform json type into dict type
    with open("student.json") as file:
        student_dict = json.loads(file.read())

    return [student_dict, school_dict]
```
<br>

#### stableMarriageAlgo file
***

```python
import loadFile
import json

def makeMatches(matches, serenaders, serenaderNames, listeners, assigned, schoolCapacity, choice):
    for serenader in serenaderNames:
        if choice == "St":
            matches[serenaders[serenader][0]].append(serenader)
            serenaders[serenader].pop(0) 

        else:
           while len(matches[serenader]) < schoolCapacity:
               matches[serenader].append(serenaders[serenader][0])
               serenaders[serenader].pop(0) 
                        
    checkMatches(matches, serenaders, listeners, assigned, schoolCapacity, choice)
    return matches

def checkMatches(matches, serenaders, listeners, assigned, schoolCapacity, choice):
    notAssignedSerenaders = []
    if choice == "St":
        for listener in sorted(list(listeners.keys())):            
            assigned = matches.get(listener)    
            if len(assigned) > schoolCapacity:
                assigned.sort(key=lambda serenader : listeners[listener].index(serenader))
                while len(assigned) != schoolCapacity:
                    notAssignedSerenaders.append(assigned.pop(-1))
    else:
        for serenader in matches.keys():
            for serenader_CheckingIteration in matches.keys():
                for listener in sorted(list(listeners.keys())):
                    if not serenader == serenader_CheckingIteration:
                        if listener in matches[serenader] and listener in matches[serenader_CheckingIteration]:
                            assigned.append(serenader)
                            assigned.append(serenader_CheckingIteration)
                            assigned.sort(key=lambda serenader : listeners[listener].index(serenader))
                            assigned = list(dict.fromkeys(assigned))
                            if listeners[listener].index(serenader) > listeners[listener].index(serenader_CheckingIteration):
                                matches[serenader_CheckingIteration].remove(listener)
                            else:
                                matches[serenader].remove(listener)
        
            if len(matches[serenader]) != schoolCapacity:
                notAssignedSerenaders.append(serenader)
    if len(notAssignedSerenaders) != 0:
        
        makeMatches(matches, serenaders, notAssignedSerenaders, listeners, assigned, schoolCapacity, choice)

def resultToJson(result):
    with open("result.json",'w') as file:
        file.write(json.dumps(result, indent=4))

def main(choice):
    schoolCapacity = 3
    studSchool = loadFile.load()
    students = studSchool[0]
    schools = studSchool[1]
    assigned = []
    matches = {}
    for school in list(schools.keys()):
        matches[school] = []
    if choice == "St":
        matches = makeMatches(matches, students, list(students.keys()), schools, assigned, schoolCapacity, choice)
    else:
        matches = makeMatches(matches, schools, list(schools.keys()), students, assigned, schoolCapacity, choice)
    resultToJson(matches)
```
<br>

### Example JSON files

#### Students JSON
***
Every **keys** of the JSON file are students and the **values** are a list of ranking schools for every students.
```json
{
    "stud0": [
        "school3",
        "school1",
        "school0",
        "school2"
    ],
    "stud1": [
        "school0",
        "school2",
        "school3",
        "school1"
    ],
    "stud2": [
        "school2",
        "school3",
        "school0",
        "school1"
    ],
    "stud3": [
        "school1",
        "school0",
        "school2",
        "school3"
    ],
    "stud4": [
        "school3",
        "school0",
        "school1",
        "school2"
    ],
    "stud5": [
        "school3",
        "school2",
        "school0",
        "school1"
    ],
    "stud6": [
        "school3",
        "school0",
        "school1",
        "school2"
    ],
    "stud7": [
        "school0",
        "school2",
        "school1",
        "school3"
    ],
    "stud8": [
        "school2",
        "school3",
        "school0",
        "school1"
    ],
    "stud9": [
        "school2",
        "school1",
        "school0",
        "school3"
    ],
    "stud10": [
        "school0",
        "school1",
        "school3",
        "school2"
    ],
    "stud11": [
        "school0",
        "school2",
        "school1",
        "school3"
    ]
}
```
<br>

#### Schools JSON
***
Every **keys** of the JSON file are schools and the **values** are a list of ranking students for every schools.
```json
{
    "school0": [
        "stud1",
        "stud5",
        "stud11",
        "stud4",
        "stud7",
        "stud8",
        "stud3",
        "stud0",
        "stud2",
        "stud10",
        "stud6",
        "stud9"
    ],
    "school1": [
        "stud8",
        "stud0",
        "stud10",
        "stud1",
        "stud6",
        "stud11",
        "stud7",
        "stud3",
        "stud9",
        "stud2",
        "stud4",
        "stud5"
    ],
    "school2": [
        "stud1",
        "stud6",
        "stud9",
        "stud10",
        "stud8",
        "stud7",
        "stud3",
        "stud11",
        "stud4",
        "stud5",
        "stud2",
        "stud0"
    ],
    "school3": [
        "stud7",
        "stud8",
        "stud2",
        "stud5",
        "stud3",
        "stud6",
        "stud1",
        "stud10",
        "stud11",
        "stud4",
        "stud9",
        "stud0"
    ]
}
```
<br>

#### Result of stable mariage JSON
***
Every **keys** of the JSON file are schools and the **values** are a list of the choosen students for every schools.
```json
{
    "school0": [
        "stud1",
        "stud11",
        "stud7"
    ],
    "school1": [
        "stud3",
        "stud10",
        "stud0"
    ],
    "school2": [
        "stud2",
        "stud8",
        "stud9"
    ],
    "school3": [
        "stud5",
        "stud6",
        "stud4"
    ]
}
```