import random
import json

def init_Students_Schools():
    nbStudents = int(input("Number of Students : "))
    nbSchools = int(input("Number of Schools : "))
    return [nbStudents,nbSchools]


def createSchools(list_init):
    random_student_list = []
    for i in range(list_init[0]):
        random_student_list.append(i)
    

    random_school_list = []
    for i in range(list_init[1]):
        random_school_list.append(i)

    # Init Students and their choices
    students_dict = {}
    for i in range(list_init[0]):
        choice = []
        random.shuffle(random_school_list)
        for j in range(len(random_school_list)):
            choice.append("school" + str(random_school_list[j]))
        students_dict["stud" + str(i)] = choice
    
    # Init Schools and their choices
    schools_dict = {}
    for i in range(list_init[1]):
        choice = []
        random.shuffle(random_student_list)
        for j in range(len(random_student_list)):
            choice.append("stud" + str(random_student_list[j]))
        schools_dict["school" + str(i)] = choice

    with open("student.json","w") as file:
        file.write(json.dumps(students_dict, indent=4))

    with open("school.json","w") as file:
        file.write(json.dumps(schools_dict, indent=4))


def main():
    createSchools(init_Students_Schools())

if __name__ == "__main__":
    main()