import loadFile
import json

# This function will make matches for a day
def makeMatches(matches, serenaders, serenaderNames, listeners, assigned, schoolCapacity, choice):
    for serenader in serenaderNames:
        if choice == "St":
            # If Students are serenading then append the current student's top ranked school list, in matches, with him
            matches[serenaders[serenader][0]].append(serenader)
            # Then remove the current student's top ranked school from his ranking list
            serenaders[serenader].pop(0) 

        else:
            # If Schools are serenading, then fill the current school's list in matches with their top ranked school
           while len(matches[serenader]) < schoolCapacity:               
               matches[serenader].append(serenaders[serenader][0])
               serenaders[serenader].pop(0) 
    
    checkMatches(matches, serenaders, listeners, assigned, schoolCapacity, choice)
    return matches

# This function will check current matches 
def checkMatches(matches, serenaders, listeners, assigned, schoolCapacity, choice):
    notAssignedSerenaders = []
    if choice == "St":        
        for listener in sorted(list(listeners.keys())):            
            # gets all students that are serenading for current school in assigned
            assigned = matches.get(listener)    
            if len(assigned) > schoolCapacity:
                # and if there are too many students in the school, sort them out following the school ranking
                assigned.sort(key=lambda serenader : listeners[listener].index(serenader))
                while len(assigned) != schoolCapacity:
                    # remove all students that are not between the first and the schoolCapacity value top ranks of the current school
                    notAssignedSerenaders.append(assigned.pop(-1))
    else:
        for serenader in matches.keys():
            for serenader_CheckingIteration in matches.keys():
                for listener in sorted(list(listeners.keys())):
                    if not serenader == serenader_CheckingIteration:
                        if listener in matches[serenader] and listener in matches[serenader_CheckingIteration]:
                            # this is to check if a student has two or more schools serenading for him at once 
                            assigned.append(serenader)
                            assigned.append(serenader_CheckingIteration)
                            assigned.sort(key=lambda serenader : listeners[listener].index(serenader))
                            assigned = list(dict.fromkeys(assigned))
                            # if a student has 2 or more schools serenading for him, remove all schools that are not his current top ranked school
                            if listeners[listener].index(serenader) > listeners[listener].index(serenader_CheckingIteration):
                                matches[serenader_CheckingIteration].remove(listener)
                            else:
                                matches[serenader].remove(listener)
        
            if len(matches[serenader]) != schoolCapacity:
                notAssignedSerenaders.append(serenader)
    if len(notAssignedSerenaders) != 0:
        # if there are still schools that are not at full capacity, call make matches with them as serenaders
        makeMatches(matches, serenaders, notAssignedSerenaders, listeners, assigned, schoolCapacity, choice)

def resultToJson(result):
    with open("result.json",'w') as file:
        file.write(json.dumps(result, indent=4))

def main(choice):
    schoolCapacity = 3
    studSchool = loadFile.load()
    students = studSchool[0]
    schools = studSchool[1]
    assigned = []
    matches = {}
    for school in list(schools.keys()):
        matches[school] = []
    if choice == "St":
        matches = makeMatches(matches, students, list(students.keys()), schools, assigned, schoolCapacity, choice)
    else:
        matches = makeMatches(matches, schools, list(schools.keys()), students, assigned, schoolCapacity, choice)
    resultToJson(matches)