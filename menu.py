import stableMarriageAlgo

def main():
    print("#############################")
    print("# Stable Marriage Algorithm #")
    print("#############################")
    print("What do you want to do ?")
    print("(St)udents serenading")
    print("(Sc)hool serenading")
    choice = input("Choice ((St) or (Sc)) : ")
    
    stableMarriageAlgo.main(choice)

if __name__ == "__main__":
    main()