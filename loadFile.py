import json

def load():
    with open("school.json") as file:
        school_dict = json.loads(file.read())

    with open("student.json") as file:
        student_dict = json.loads(file.read())

    return [student_dict, school_dict]